﻿
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

public class Movement : NetworkBehaviour
{
    [Header("prefab")]
    [SerializeField] private GameObject _bomb = null;
    [SerializeField] private TextMesh _lifeText = null;
    [SerializeField] private Animator _animator = null;

    private Rigidbody2D _rb = null;
    private int _range = 2;
    [SerializeField][SyncVar] private float _speed = 3;
    [SerializeField] private bool _canDropBombs = true;

    [SerializeField][SyncVar(hook = nameof(updateLife))] private int _health = 3;
    [SyncVar]private string _textHud = ""; 

    private void updateLife(int oldValue, int newValue)
    {
        _textHud = newValue + "";
        _lifeText.text = _textHud;
    }

    [SerializeField] [SyncVar] private int _bombs = 1;
    [SerializeField] private bool _invulnerable = false;


    public int Health { get => _health; set => _health = value; }
    public float Speed { get => _speed; set => _speed = value; }
    public int Bombs { get => _bombs; set => _bombs = value; }
    public bool Invulnerable { get => _invulnerable; set => _invulnerable = value; }
    public bool CanDropBombs { get => _canDropBombs; set => _canDropBombs = value; }
    public int Range { get => _range; set => _range = value; }

    public override void OnStartLocalPlayer()
    {
        _rb = this.gameObject.GetComponent<Rigidbody2D>();
        _bomb = Resources.Load("Prefabs/Bomb") as GameObject;
        cmndInstantiate(3);
    }

    [Command]
    private void cmndInstantiate(int life)
    {
        this._health = life;
    }

    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            _rb.velocity = new Vector3(horizontal, vertical, 0) * _speed;

            if (Input.GetKeyDown(KeyCode.W))
            {
                _animator.SetBool("upwards", true);
            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                _animator.SetBool("upwards", false);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                _animator.SetBool("downwards", true);
            }
            else if(Input.GetKeyUp(KeyCode.S))
            {
                _animator.SetBool("downwards", false);
            }
            else if (Input.GetKeyDown(KeyCode.A))
            {
                _animator.SetBool("leftwards", true);
                transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
            else if (Input.GetKeyUp(KeyCode.A))
            {
                _animator.SetBool("leftwards", false);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                _animator.SetBool("rightwards", true);
                transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().flipX = false;
            }
            else if (Input.GetKeyUp(KeyCode.D))
            {
                _animator.SetBool("rightwards", false);
            }

            if ((_bombs > 0) && Input.GetKeyDown(KeyCode.Space))
            {
                DropBomb();
            }
        }
    }

    [Command]
    private void DropBomb()
    {
        Vector3 positionBomb = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y), 0);
        GameObject bomb = Instantiate(_bomb);
        bomb.transform.position = positionBomb;
        NetworkServer.Spawn(bomb);
            
        bomb.GetComponent<BombBehavoiur>().Expantion = Range;
        StartCoroutine(BombCooldown());
    }

    [ServerCallback]
    public void Damage()
    {
        if (_health > 1)
        {
            _health--;
        }
        else
        {
            _health = 0;
            Destroy(this.gameObject);
        }
    }

    [ServerCallback]
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "fire" && !_invulnerable)
        {
            Damage();
            StartCoroutine(makeInvulnerable());
        }
    }

    IEnumerator makeInvulnerable()
    {
        _invulnerable = true;
        yield return new WaitForSeconds(1f);
        _invulnerable = false;
    }

    IEnumerator BombCooldown()
    {
        _bombs--;
        if (_bombs == 0) { _canDropBombs = false; }
        yield return new WaitForSeconds(2f);
        _bombs++;
        _canDropBombs = true;
    }

}
