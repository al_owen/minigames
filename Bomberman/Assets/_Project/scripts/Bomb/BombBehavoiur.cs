﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using Mirror;

public class BombBehavoiur : NetworkBehaviour
{
    [SerializeField] private GameObject _fire = null;
    [SerializeField] private LayerMask _layer;
    [SerializeField] private int _expantion = 3;

    public int Expantion { get => _expantion; set => _expantion = value; }

    private void Awake()
    {
        _fire = Resources.Load("Prefabs/flame") as GameObject;
    }

    private void Start()
    {
        StartCoroutine(bombExplotion(Vector2.up));
        StartCoroutine(bombExplotion(Vector2.down));
        StartCoroutine(bombExplotion(Vector2.left));
        StartCoroutine(bombExplotion(Vector2.right));
    }

    [ServerCallback]
    public void commandExplode(Vector3 place)
    {
        PlaceFire(place);
    }

    [ServerCallback]
    public void PlaceFire(Vector3 place)
    {
        GameObject fire = Instantiate(_fire);
        fire.transform.position = place;
        NetworkServer.Spawn(fire);
    }

    IEnumerator bombExplotion(Vector3 direction)
    {
        yield return new WaitForSeconds(1.7f);

        for (int i = 1; i < _expantion; i++)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, i, _layer);
            if(!hit.collider)
            {
                Vector3 place = transform.position + (i * direction);
                commandExplode(place);
            }
            else
            {
                break;
            }
            
        }
        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);
    }
}
