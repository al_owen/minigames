﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class boton : MonoBehaviour
{
    public void inicio()
    {
        GameManager.Instance.GameStarted = true;
        SceneManager.LoadScene("Game");
    }
    public void salir()
    {
        Application.Quit();
    }
}
