﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralTerrain : MonoBehaviour
{
    [Header("Noise variables")]
    [SerializeField] int _amplitude;
    [Range(1, 1000)] [SerializeField] float _frequency;

    [Header("Seeds")]
    [SerializeField] float _seedX;
    [SerializeField] float _seedY;

    [Header("Map")]
    [SerializeField] int _width;
    [SerializeField] int _depth;

    [Header("Terrain")]
    [SerializeField] TerrainData _tData;

    [Header("Octave")]
    [SerializeField] int _octave;
    [SerializeField] float _lacunarity;
    [Range(0, 1)][SerializeField] float _persistence;
    [SerializeField] Vector2 _offset;

    [Header("Inspector")]
    public bool inspectorUpdate;
    public bool randomize;

    // Start is called before the first frame update
    void Awake()
    {
        generateMap();
    }

    public void generateMap()
    {
        _tData.heightmapResolution = _width;
        _tData.size = new Vector3(_width, _amplitude, _depth);
        _tData.SetHeights(0, 0, generateNoiseMap());
    }

    public float[,] generateNoiseMap()
    {
        float[,] heights = new float[_width, _depth];

        Vector2 realOffset = new Vector2();
        Vector2[] offSetOctaves = new Vector2[_octave];

        for (int i = 0; i < _octave; i++)
        {
            if (randomize)
            {
                _seedX = Random.Range(-100000, 100000);
                _seedY = Random.Range(-100000, 100000);
            }
            realOffset.x = _seedX + _offset.x;
            realOffset.y = _seedY + _offset.y;
            offSetOctaves[i] = realOffset;
        }

        _frequency = Mathf.Clamp(_frequency, 0.0001f, 1000f);

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float halfWidth = _width / 2f;
        float halfHeight = _depth / 2f;

        for (int y = 0; y < _depth; y++)
        {
            for (int x = 0; x < _width; x++)
            {

                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < _octave; i++)
                {

                    float sampleX = (x - halfWidth) / _frequency * frequency + offSetOctaves[i].x;
                    float sampleY = (y - halfHeight) / _frequency * frequency + offSetOctaves[i].y;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;

                    amplitude *= _persistence;
                    frequency *= _lacunarity;

                }

                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }
                heights[x, y] = noiseHeight;
            }
        }

        for (int y = 0; y < _depth; y++)
        {
            for (int x = 0; x < _width; x++)
            {
                heights[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, heights[x, y]);
            }
        }

        return heights;
    }

}
