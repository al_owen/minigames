﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private Text _scoreTxt;
    [SerializeField] private Text _hpTxt;

    [SerializeField] private GameObject _dieWindow;
    [SerializeField] private Text _dieTxt;


    void Start()
    {
        _scoreTxt = transform.GetChild(0).gameObject.GetComponent<Text>();
        _hpTxt = transform.GetChild(1).gameObject.GetComponent<Text>();
        _dieWindow = transform.GetChild(2).gameObject;
        _dieTxt = _dieWindow.transform.GetChild(0).gameObject.GetComponent<Text>();
        GameManager.Instance.change += UpdateScore;
        GameManager.Instance.die += DeathScreen;
        Navesota.hpChange += UpdateHp;
    }

    public void UpdateScore(int score)
    {
        _scoreTxt.text = "Score: " + score;
    }

    public void UpdateHp(int hp)
    {
        _hpTxt.text = "Hp: " + hp;
    }

    public void DeathScreen()
    {
        GameManager.Instance.change -= UpdateScore;
        GameManager.Instance.die -= DeathScreen;
        Navesota.hpChange -= UpdateHp;
        StartCoroutine(death());
    }

    IEnumerator death()
    {
        print("Im dead");
        _dieWindow.SetActive(true);
        _dieTxt.text = "Your score was " + GameManager.Instance.Score;
        yield return new WaitForSeconds(2f);
        _dieWindow.SetActive(false);
    }

}
