﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class powerUps : NetworkBehaviour
{
    public enum powerTypes
    {
        bombUp, fireUp, liveUp, speedUp, attackUp
    }
    public powerTypes types;

    [ServerCallback]
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player")) {
            Movement player = collision.gameObject.GetComponent<Movement>();
            effect(player);
            Destroy(this.gameObject);
        }
    }

    private void effect(Movement player)
    {
        switch (types)
        {
            case powerTypes.fireUp:
                player.Range++;
                break;
            case powerTypes.speedUp:
                player.Speed += 0.5f;
                break;
            case powerTypes.liveUp:
                player.Health++;
                break;
            case powerTypes.bombUp:
                player.Bombs++;
                break;
        }
    }
}
