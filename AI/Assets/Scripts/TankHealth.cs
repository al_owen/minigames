﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankHealth : MonoBehaviour
{
    #region variables
    [SerializeField] int _maxLife = 10;
    [SerializeField] int _life;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        respawn();
    }

    public void beDamaged()
    {
        print(transform.tag + " has been hit");
        _life--;
        if (_life >= 1)
        {
            this.gameObject.GetComponent<TankAI>().AddReward(-2f); //si le disparan lo castigo para que intente evitar ser disparado
        }
        else
        {
            this.gameObject.GetComponent<TankAI>().PunishTank(-5f); //si lo matan lo castigo más para que intente evitar ser disparado
        }
    }

    private void respawn()//restauro la vida
    {
        _life = _maxLife;
    }
}
