﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public TankAI[] tanks;


    // Start is called before the first frame update
    void Start()
    {
        foreach(TankAI tank in tanks)
        {
            tank.EnemyHit += ChangeGround; //suscribo ground al evento que se llama cuando se dispara a un enemigo
        }
    }

    public void ChangeGround(Color color)
    {
        this.GetComponent<MeshRenderer>().material.color = color; //se pide un valor para cambiar el color del suelo
    }
   
}
