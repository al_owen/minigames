﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Unity;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Navesota _player;
    [SerializeField] private bool _canSpawn;
    [SerializeField] private bool _gameOver = false;
    [SerializeField] private bool _gameStarted = false;
    private int _score;

    public delegate void onScoreChange(int score);
    public event onScoreChange change;

    public delegate void onDeath();
    public event onDeath die;

    public static GameManager Instance;

    public bool GameOver { get => _gameOver; set => _gameOver = value; }
    public int Score { get => _score; set => _score = value; }
    public bool GameStarted { get => _gameStarted; set => _gameStarted = value; }
    public Navesota Player { get => _player; set => _player = value; }

    // Start is called before the first frame update
    void Awake()
    {
        if(Instance != null && Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public static Vector3 GetPlayerPosition()
    {
        if (Instance.Player != null)
        {
            return Instance._player.transform.position;
        }
        else 
        {
            return Instance.transform.position;
        }
    }

    public static float getRandomRot()
    {
        return Random.Range(0, 360);
    }

    public void addPoints(int points)
    {
        _score += points;
        if(change != null)
        {
            change.Invoke(_score);
        }
    }

    public void Damage(int hpDamage)
    {
        _player.damage(hpDamage);
    }

    public void volverMenu()
    {
        StartCoroutine(loadScene());
    }

    IEnumerator loadScene()
    {
        yield return new WaitForSeconds(5f);
        Instance.GameOver = false;
        Instance.GameStarted = false;
        SceneManager.LoadScene("menu");
    }
}
