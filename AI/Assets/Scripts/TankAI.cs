﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class TankAI : Agent
{
    #region Variables
    public delegate void OnChangeEpisode(Color color);
    public event OnChangeEpisode EnemyHit;

    [Header("Movement values")]
    [SerializeField] float _shootRange = 30;
    [SerializeField] float _speed = 20;
    [SerializeField] float _rotationSpeed = 25;

    [Header("objects")]
    [SerializeField] GameObject _turret;
    [SerializeField] GameObject _shootingPoint;

    [Header("transforms")]

    [SerializeField] Transform _target1, _target2;
    [SerializeField] Transform _ally;

    [Header("special values")]
    [SerializeField] bool heuristics = false;
    
    Rigidbody _rb;
    Vector3 _initialPos;
    Vector3 _initialRot;
    float _hitReward;

    #endregion


    public override void Initialize()
    {
        //inicializamos los valores
        _rb = gameObject.GetComponent<Rigidbody>(); 
        _initialPos = this.transform.localPosition;
        _initialRot = this.transform.eulerAngles;
        _hitReward = 10f;
    }

    public override void OnEpisodeBegin()
    {
        //reiniciamos los valores
        _initialPos.x = Random.Range(-40, 40);
        transform.localPosition = _initialPos;
        transform.eulerAngles = _initialRot;
        _rb.velocity = Vector3.zero;
        _hitReward = 10f;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(_target1.position);
        sensor.AddObservation(_target2.position);
        sensor.AddObservation(_ally.position);
        sensor.AddObservation(transform.position);
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0]; //cojo el primer elemento para la rotacion
        float moveZ = actions.ContinuousActions[1]; //cojo el segundo elemento para movernos

        if(Mathf.RoundToInt(actions.ContinuousActions[2]) >= 1) Shoot(); //cojo el tercer elemento, lo redondeo y si es mayor o igual a 1 dispara
        AddReward( ((moveZ < 0)? -0.1f : 0.15f) );//si el personaje se mueve hacia atras lo penalizo, si se mueve para adelante lo recompenso un poco mas

        Vector3 wantedPosition = transform.position + (transform.forward * moveZ * _speed * Time.deltaTime);
        _rb.MovePosition(wantedPosition); //movimeinto hacia adelante

        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0f, _rotationSpeed * Time.deltaTime * moveX, 0f)); //rotacion del tanque

    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> actions = actionsOut.ContinuousActions;

        if (heuristics) //esto es para decirle a que tanque quiero mover si estoy en modo heuristics, para no mover todos al mismo tiempo
        {
            actions[0] = Input.GetAxis("Horizontal");
            actions[1] = Input.GetAxis("Vertical");
            actions[2] = Input.GetKey(KeyCode.Space) ? 1f: 0f; //simulo un bool si presiono la barra espaciadora devuelve uno sino 0
        }

    }

    private void Shoot()
    {
        var layerMask = 1 << LayerMask.NameToLayer("Tanks"); //cojo el layer en el que las balas
        var dir = _shootingPoint.transform.right; //calculo la direccion
        Debug.DrawRay(_shootingPoint.transform.position, dir * _shootRange, Color.yellow, 0.5f);

        RaycastHit hit;
        if (Physics.Raycast(_shootingPoint.transform.position, dir, out hit, _shootRange, layerMask))
        {
            if (hit.transform.tag == "RedTeam" && this.transform.tag == "BlueTeam") //si son del equipo rival los premio con varios puntos
            {
                hit.transform.gameObject.GetComponent<TankHealth>().beDamaged();
                _hitReward += 1f; //hago la recompensa progresiva cada vez que dispara a un rival
                AddReward(_hitReward);
                EnemyHit.Invoke(Color.green);  //llamo al evento y le paso el color verde para que el suelo se haga verde cuando dispara a un enemigo
            }
            else if (hit.transform.tag == "RedTeam" && this.transform.tag == "RedTeam")
            {
                AddReward(-8.5f); //lo castigo si dispara a un aliado
                EnemyHit.Invoke(Color.red); //llamo al evento y le paso el color verde para que el suelo se haga rojo cuando dispara a un aliado
                print("Friendly Fire! "+ this.transform.tag);
            }
            else if (hit.transform.tag == "BlueTeam" && this.transform.tag == "RedTeam")
            {
                hit.transform.gameObject.GetComponent<TankHealth>().beDamaged();
                _hitReward += 1f;
                AddReward(_hitReward);
                EnemyHit.Invoke(Color.green);
            }
            else if (hit.transform.tag == "BlueTeam" && this.transform.tag == "BlueTeam")
            {
                AddReward(-8.5f);
                EnemyHit.Invoke(Color.red);
                print("Friendly Fire! " + this.transform.tag);
            }
            else
            {
                AddReward(-3f);// le resto un poco para que no haga spam de tiros, pero le da un poco igual, debo revisarlo
            }

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Wall") //lo castigo si colisiona con una pared
        {
            PunishTank(-8f);
            EnemyHit.Invoke(Color.red);
        }
        else if (collision.transform.tag == "BlueTeam" && this.transform.tag == "BlueTeam")//lo castigo si se choca contra un tanque ya sea aliado o enemigo(mas castigo si es aliado)
        {
            PunishTank(-4f);
            EnemyHit.Invoke(Color.red);
            print("Careful! "+this.transform.tag);
        }
        else if (collision.transform.tag == "BlueTeam" && this.transform.tag == "RedTeam")
        {
            PunishTank(-3f);
            EnemyHit.Invoke(Color.red);
        }
        else if (collision.transform.tag == "RedTeam" && this.transform.tag == "RedTeam")
        {
            PunishTank(-4f);
            EnemyHit.Invoke(Color.red);
            print("Careful! " + this.transform.tag);
        }
        else if (collision.transform.tag == "RedTeam" && this.transform.tag == "BlueTeam")
        {
            PunishTank(-3f);
            EnemyHit.Invoke(Color.red);
        }
    }

    public void PunishTank(float reward) //para ahorrarme una linea cuando lo castigo...
    {
        AddReward(reward);
        EndEpisode();
    }

}
