﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Extinguish : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(extinguishFire());
    }

    IEnumerator extinguishFire()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }
}
