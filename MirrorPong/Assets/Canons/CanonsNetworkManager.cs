﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonsNetworkManager : NetworkManager
{
    public Transform P1;
    public Transform P2;
    public Transform P3;
    public Transform P4;
    public bool side = false;

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        // add player at correct spawn position
        Transform start = P1;
        if (numPlayers == 0)
        {
            start = P1;
            side = false;
        }
        else if (numPlayers == 1)
        {
            start = P2;
            start.Rotate(new Vector3(0, 0, 180));
            side = false;
        }
        else if (numPlayers == 2)
        {
            start = P3;
            start.Rotate(new Vector3(0, 0, 270));
            side = true;
        }
        else if (numPlayers == 3)
        {
            start = P4;
            start.Rotate(new Vector3(0, 0, 90));
            side = true;
        }

        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        player.GetComponent<Player>().side = side;
        NetworkServer.AddPlayerForConnection(conn, player);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        // call base functionality (actually destroys the player)
        base.OnServerDisconnect(conn);
    }
}
