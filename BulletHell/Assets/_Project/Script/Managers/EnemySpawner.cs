﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using System;
using Unity.Collections;
using Unity.Transforms;
using Unity.Mathematics;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [Header("Spawner Variables")]
    [SerializeField] private int _totalEnemies = 100;
    [SerializeField] private Vector3 _bounds;
    [SerializeField] private int _spawnDelay = 2;
    [SerializeField] private float _minSpeed = 1f;
    [SerializeField] private float _maxSpeed = 5f;
    [SerializeField] private float _spawnTime = 2f;

    [Header("Prefabs")]
    [SerializeField] private GameObject _enemyYellow, _enemyBlue, _enemyRed, _enemyLila = null;
    
    //entities
    private EntityManager _entityManager = default;
    private Entity _entityY, _entityB, _entityR, _entityL = default;

    //singleton
    public static EnemySpawner Instance;

    void Awake()
    {
        if(Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        init();
        StartCoroutine(Spawn());
    }

    private void init()
    {
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        _entityY = GameObjectConversionUtility.ConvertGameObjectHierarchy(_enemyYellow, settings);
        _entityB = GameObjectConversionUtility.ConvertGameObjectHierarchy(_enemyBlue, settings);
        _entityR = GameObjectConversionUtility.ConvertGameObjectHierarchy(_enemyRed, settings);
        _entityL = GameObjectConversionUtility.ConvertGameObjectHierarchy(_enemyLila, settings);
    }

    private void SpawnEntity(Entity entity)
    {
        int side = Random.Range(0, 2);
        Vector3 myPos = this.transform.position;

        Quaternion rot = transform.rotation;
        float3 pos= new float3(myPos.x, myPos.y, myPos.z);

        NativeArray<Entity> enemies = new NativeArray<Entity>(_totalEnemies, Allocator.Temp);
        for (int i = 0; i <enemies.Length; i++)
        {
            float rand = Random.Range(-8f, 8f);
            if (side == 1)
            {
                pos = new float3(rand, myPos.y, 0f);
            }
            else
            {
                pos = new float3(rand, -10f, 0f);
                rot = Quaternion.Euler(0f, 0f, 0f);
            }

            enemies[i] = _entityManager.Instantiate(entity);
            _entityManager.SetComponentData(enemies[i], new Translation { Value = pos});
            _entityManager.SetComponentData(enemies[i], new Rotation { Value = rot});
            _entityManager.SetComponentData(enemies[i], new MoveForward { speed = Random.Range(_minSpeed, _maxSpeed) });
        }
        enemies.Dispose();
    }


    IEnumerator Spawn()
    {
        while (!GameManager.Instance.GameOver)
        {
            yield return new WaitForSeconds(_spawnTime);
            SpawnEntity(_entityY);
            SpawnEntity(_entityB);
            SpawnEntity(_entityR);
            SpawnEntity(_entityL);
        }
    }
}
