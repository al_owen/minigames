﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnClickListener : MonoBehaviour
{
    public void OnClickStart()
    {
        SceneManager.LoadScene("OfflineScene");
    }

    public void OnClickVolverInicio()
    {
        SceneManager.LoadScene("Intro");
    }

    public void OnClickVolverSeleccion()
    {
        SceneManager.LoadScene("SeleccionJugable");
    }

    public void OnClickGoCrearPartida()
    {
        SceneManager.LoadScene("SeleccionPersonajes");
    }

    public void OnClickGoUnirsePartida()
    {
        SceneManager.LoadScene("EntrarJugador");
    }
}
