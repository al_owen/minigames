﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BrickSpawner : NetworkBehaviour
{
    [SerializeField] private GameObject _breakable = null;
    [SerializeField] private int _frequency = 6;
    [SerializeField] private GameObject[] arrayPowerUps = null;
    [SerializeField] private int _frequencyUps = 5;

    private int _limitX1 = -10;
    private int _limitX2 = 12;
    private int _limitY1 = 7;
    private int _limitY2 = -5;

    [ServerCallback]
    public void Spawner()
    {
        int spotL = _limitX1 + 1;
        int spotR = _limitX2 - 1;
        int spotU = _limitY1 - 1;
        int spotD = _limitY2 + 1;

        for (int i = _limitX1; i <= _limitX2; i++)
        {
            for (int j = _limitY2; j <= _limitY1; j++)
            {
                int rand = Random.Range(0, 10);
                if (rand < _frequency)
                {
                    if ((i == _limitX1 && (j > spotD && j < spotU)) ||
                        (i == _limitX2 && (j > spotD && j < spotU)) ||
                        (j == _limitY1 && (i > spotL && i < spotR)) ||
                        (j == _limitY2 && (i > spotL && i < spotR)) ||
                        (i != _limitX1 && i != _limitX2 && j != _limitY1 && j != _limitY2))
                    {
                        if (((i % 2) == 0) && ((j % 2) != 0) || ((i % 2) == 0) && ((j % 2) == 0) || ((i % 2) != 0) && ((j % 2) != 0))
                        {
                            int random = Random.Range(0, 11);
                            if (random < _frequencyUps)
                            {
                                //Instantiate(arrayPowerUps[Random.Range(0, arrayPowerUps.Length)]);
                                int randomups = Random.Range(0, arrayPowerUps.Length);

                                GameObject powerUps = Instantiate(arrayPowerUps[randomups]);
                                powerUps.transform.position = new Vector2(i, j);
                                NetworkServer.Spawn(powerUps);
                            }
                            
                            GameObject brick = Instantiate(_breakable);
                            brick.transform.position = new Vector2(i, j);
                            NetworkServer.Spawn(brick);
                        }
                    }
                }
                
            }
        }
    }

}
