﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Jobs;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class Movement : SystemBase
{
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();
        
        float dt = Time.DeltaTime;
        
        Entities.WithAny<EnemyTag, BulletTag>().ForEach(
        (Entity entity, int entityInQueryIndex, ref Translation trans, in Rotation rot, in MoveForward moveForward) =>
        {
            var direction = math.mul(rot.Value, math.up());

            trans.Value += moveForward.speed * dt * direction;
        }
        ).ScheduleParallel();
    }
}
