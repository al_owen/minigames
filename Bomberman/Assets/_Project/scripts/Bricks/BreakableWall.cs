﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BreakableWall : NetworkBehaviour
{
    [SyncVar] public bool reinforced = false;
    [SyncVar] bool invul = false;
    public override void OnStartServer()
    {
        if (Random.Range(0, 10) < 2)
        {
            reinforced = true;
        }
    }

    [ServerCallback]
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "fire" && !invul)
        {
            if (reinforced)
            {
                reinforced = false;
                changeColor();
                StartCoroutine(invulnerable());
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

    }

    [ClientRpc]
    void changeColor()
    {
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.gray;
    }

    IEnumerator invulnerable ()
    {
        invul = true;
        yield return new WaitForSeconds(1f);
        invul = false;
    }

}
