﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character")]
public class STOCharacter : ScriptableObject
{
    
    public GameObject player;
    public string characterName;
    [SerializeField] private int life;
    [SerializeField] private int maxLife = 10;
    public int blowRange;
    public float speed;
    public int bombs;
    public bool dead;
    public bool invulnerable;

    public void restart()
    {
        dead = false;
        life = maxLife;
        invulnerable = false;
    }

    public int Life { get => life; set => life = value; }

    public void getDamaged()
    {
        if (life > 0)
        {
            life--;
        }
        else
        {
            dead = true;
        }
    }

}
