﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class DestroySystem : SystemBase
{
    float time;

    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    protected override void OnCreate()
    {
        time = Time.DeltaTime;

    }

    protected override void OnUpdate()
    {
        var bounds = 15f;

        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();

        Entities.WithAny<EnemyTag, BulletTag>().ForEach((Entity entity, int entityInQueryIndex, in Translation trans) =>
        {
            if (trans.Value.x < -bounds || trans.Value.x > bounds || trans.Value.y < -bounds || trans.Value.y > bounds)
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, entity);
            }
        }
        ).WithName("destroyjob").ScheduleParallel();

       
        barrier.AddJobHandleForProducer(Dependency);

    }
}
