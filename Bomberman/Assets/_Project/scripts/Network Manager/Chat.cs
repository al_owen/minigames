﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Mirror;

public class Chat : NetworkBehaviour
{
    [SerializeField] private GameObject _chatUI = null;
    [SerializeField] private Text _chatText = null;
    [SerializeField] private InputField _input = null;

    private static event Action<string> onMessage;

    public override void OnStartAuthority()
    {
        _chatUI.SetActive(true);
        onMessage += handleNewMessage;
    }

    [Client]
    private void OnDestroy()
    {
        if (!hasAuthority) { return; }
        onMessage -= handleNewMessage;
    }

    private void handleNewMessage(string message)
    {
        _chatText.text += message;
    }

    [Client]
    public void send(string message)
    {
        if (!Input.GetKeyDown(KeyCode.Return)) { return; }
        if (string.IsNullOrWhiteSpace(message)) { return; }

        cmdSendMessage(message);
        _input.text = string.Empty;
    }

    [Command]
    private void cmdSendMessage(string message)
    {
        rpcHandlerMessage($"[{this.netIdentity}]: {message}");
    }

    [ClientRpc]
    private void rpcHandlerMessage(string message)
    {
        onMessage?.Invoke($"\n{message}");
    }
}
