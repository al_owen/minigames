﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProceduralPainting))]
public class PaintingEditor : Editor
{
	public override void OnInspectorGUI()
	{
		ProceduralPainting paint = (ProceduralPainting)target;
		DrawDefaultInspector();

		if (GUILayout.Button("Paint"))
		{
			paint.paintMap();
		}
	}
}
