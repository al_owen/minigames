﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProceduralTerrain))]
public class ProceduralEditor : Editor
{
	public override void OnInspectorGUI()
	{
		ProceduralTerrain terrain = (ProceduralTerrain) target;
		

		if (DrawDefaultInspector())
		{
			if (terrain.inspectorUpdate)
			{
				terrain.generateMap();
			}
		}

		if (GUILayout.Button("Generate"))
		{
			terrain.generateMap();
		}
	}
}
