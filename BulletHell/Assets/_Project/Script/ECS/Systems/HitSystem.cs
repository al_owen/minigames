﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class HitSystem : ComponentSystem
{
    private float collisionDistance = 0.3f;
    private float playercollisionDistance = 0.5f;

    protected override void OnUpdate()
    {
        if (GameManager.Instance.GameOver || !GameManager.Instance.GameStarted)
        {
            return;
        }

        float3 playerPos = (float3)GameManager.GetPlayerPosition();

        Entities.WithAll<EnemyTag>().ForEach((Entity enemy, ref Translation enemyPos) =>
        {
            playerPos.z = enemyPos.Value.z;
            if (math.distance(playerPos, enemyPos.Value) < playercollisionDistance)
            {
                PostUpdateCommands.DestroyEntity(enemy);
                GameManager.Instance.Damage(1);
            }

            float3 enemyPosition = enemyPos.Value;

            Entities.WithAll<BulletTag>().ForEach((Entity bullet, ref Translation bulletPos) =>
            {
                if (math.distance(bulletPos.Value, enemyPosition) < collisionDistance)
                {
                    PostUpdateCommands.DestroyEntity(enemy);
                    PostUpdateCommands.DestroyEntity(bullet);

                    GameManager.Instance.addPoints(1);
                }
            });


        });
    }
}
