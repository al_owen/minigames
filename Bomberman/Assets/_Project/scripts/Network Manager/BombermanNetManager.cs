﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Mirror;

public class BombermanNetManager : NetworkManager
{
    public Transform spawn1, spawn2, spawn3, spawn4 = null;
    public BrickSpawner spawner = null;

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        Transform start = spawn1;
        if (numPlayers == 0)
        {
            start = spawn1;
        }
        else if (numPlayers == 1)
        {
            start = spawn2;
        }
        else if (numPlayers == 2)
        {
            start = spawn3;
        }
        else if (numPlayers == 3)
        {
            start = spawn4;
        }

        GameObject player = Instantiate(playerPrefab, start.position, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, player);
        if(numPlayers == 2)
        {
            spawner.Spawner();
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
    }
}
