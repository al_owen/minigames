﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableTrigerOnLaid : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GetComponent<Collider2D>().isTrigger = false; // Disable the trigger
        }
    }

    private void OnDisable()
    {
        GetComponent<Collider2D>().isTrigger = true; //Enable the trigger
    }
}
