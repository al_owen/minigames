﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralG : MonoBehaviour
{
    public GameObject[] blocks;
    private GameObject _currentBlock;

    [SerializeField] private int _seed = 987343;
    [SerializeField] private float _amp = 150f;
    [SerializeField] private float _freq = 100f;

    [SerializeField] private int _whiteNoise = 33;


    [SerializeField] int _row = 80;
    [SerializeField] int _col = 50;

    // Start is called before the first frame update
    void Awake()
    {
        gererateProceduralMap();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            
            gererateProceduralMap();
        }
    }

    private void gererateProceduralMap()
    {
        Vector2 pos = this.transform.position;
       
        for (int x = 0; x < _col; x++)
        {
            for (int y = 0; y < _row; y++)
            {
                float f = Mathf.PerlinNoise((pos.x + x + _seed) / _freq, (pos.y + y) / _freq);

                f *= _amp;

                f -= 1;

                if (f > _whiteNoise * (_amp / 100))
                {
                    _currentBlock = blocks[1];
                }
                else
                {
                    _currentBlock = blocks[0];
                }

                GameObject newBlock = Instantiate(_currentBlock);
                newBlock.transform.position = new Vector3(pos.x + x, pos.y + y);

            }
        }
    }

}
