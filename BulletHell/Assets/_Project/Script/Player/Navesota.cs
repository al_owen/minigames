﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


public class Navesota : MonoBehaviour
{
    public int hpMax;
    private int hp;
    public int spd;
    private Vector3 movement;
    private Camera camera;

    [SerializeField] private GameObject _bullets;
    [SerializeField] private Transform _cannon1, _cannon2;
    private EntityManager _entityManager = default;
    private Entity _entityPrefab = default;
    private bool _coolDown = true;

    //delegates
    public delegate void onHpChange(int score);
    public static event onHpChange hpChange;

    // Start is called before the first frame update
    void Start()
    {
        hp = hpMax;
        camera = Camera.main;
        init();
        GameManager.Instance.Player = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.GameOver)
        {
            //float moveHorizontal = Input.GetAxisRaw("Horizontal");
            float moveVertical = Input.GetAxisRaw("Vertical");

            movement = transform.up  * moveVertical;
            movement = movement * spd * Time.deltaTime;

            transform.position += movement;

            Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPosition.z = 0;

            Vector3 lookAtDirection = mouseWorldPosition - transform.position;
            transform.up = lookAtDirection;
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && !GameManager.Instance.GameOver)
        {
            shoot(_cannon1);
            //shoot(_cannon2);
        }
    }


    private void init()
    {
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        _entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(_bullets, settings);
    }

    private void shoot(Transform cannon)
    {
        if (_coolDown)
        {
            NativeArray<Entity> enemies = new NativeArray<Entity>(1, Allocator.Temp);
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i] = _entityManager.Instantiate(_entityPrefab);
                _entityManager.SetComponentData(enemies[i], new Translation { Value = cannon.position });
                _entityManager.SetComponentData(enemies[i], new Rotation { Value = cannon.rotation });
                _entityManager.SetComponentData(enemies[i], new MoveForward { speed =  7f});
            }
            enemies.Dispose();
        }
    }

    public void damage(int damagePoints)
    {
        if (hp > 0)
        {
            hp--;
        }
        else
        {
            hp = 0;
            GameManager.Instance.GameOver = true;
            GameManager.Instance.volverMenu();
        }
        hpChange.Invoke(hp);

    }
    
}
