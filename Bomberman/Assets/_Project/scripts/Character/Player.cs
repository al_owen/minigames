﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Player : NetworkBehaviour
{
    //public STOCharacter character;
    private int _health = 3;
    private int _speed = 2;
    private int _bombs = 1;
    private bool _invulnerable = false;
    private bool _canDropBombs;

    public int Health { get => _health; set => _health = value; }
    public int Speed { get => _speed; set => _speed = value; }
    public int Bombs { get => _bombs; set => _bombs = value; }
    public bool Invulnerable { get => _invulnerable; set => _invulnerable = value; }
    public bool CanDropBombs { get => _canDropBombs; set => _canDropBombs = value; }


    public void Damage()
    {
        _health--;
        if(_health < 0)
        {
            _health = 0;
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "fire" && !_invulnerable)
        {
            Damage();
            StartCoroutine(makeInvulnerable());
        }
    }

    IEnumerator makeInvulnerable()
    {
        _invulnerable = true;
        yield return new WaitForSeconds(1f);
        _invulnerable = false;
    }
}
