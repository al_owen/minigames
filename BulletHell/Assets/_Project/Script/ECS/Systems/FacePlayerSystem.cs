﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class FacePlayerSystem : SystemBase
{
    float time;
    
    protected override void OnCreate()
    {
        time = Time.DeltaTime;
    }

    protected override void OnUpdate()
    {
            if (GameManager.Instance.GameOver)
            {
                return;
            }

            float3 playerPos = (float3)GameManager.GetPlayerPosition();
            float rand = GameManager.getRandomRot();

            Entities.WithAny<EnemyTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, ref Rotation rot) =>
            {
            
                float3 direction = playerPos - trans.Value;
                direction.x = 0f;
                direction.y = 0f;

                rot.Value = Quaternion.LookRotation(math.forward(rot.Value), direction);

            }).ScheduleParallel();
         
    }
}
